package rubotlin

object Root {
    fun sayHello() = "Hello world from Rubotlin!"

    data class Example(val one: Int, val two: String)

    fun giveMeObject() = Example(1, "two")
}

/*
fun potato() {
    console.log(Root.sayHello())
}

fun main() {
    potato()
}*/