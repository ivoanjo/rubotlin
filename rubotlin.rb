require "bundler/inline"

gemfile do
  source "https://rubygems.org"
  gem "execjs"
  gem "pry"
end

require "execjs"

kotlin_js = File.read("build/js/packages_imported/kotlin/1.3.72/kotlin.js")
rubotlin_js = File.read("build/js/packages/rubotlin/kotlin/rubotlin.js")

context = ExecJS.compile(kotlin_js + rubotlin_js)
puts context.eval("rubotlin.rubotlin.Root.sayHello()")

require 'pry'
binding.pry
